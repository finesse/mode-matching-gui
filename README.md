# Mode matching tool proof of concept

## Usage

```
python -m gui
```

## Requirements

Finesse 3 and `pyside2`.

## To-do

- Make laser/first component fixed, or at least stop the error when it is moved.
- Allow properties like focal lengths to be set.
- Add proper graphics for components.
- Add ability to insert components
  - "Thick" components with multiple surfaces and constrained space between (substrate)

## Credits
Sean Leavey  
<sean.leavey@ligo.org>
