"""GUI."""

from functools import total_ordering, cached_property

from PySide2.QtUiTools import QUiLoader
from PySide2.QtCore import (
    Signal,
    QObject,
    Qt,
    QFile,
    QMarginsF,
    QLineF,
    QPointF,
    QItemSelectionModel,
)
from PySide2.QtGui import (
    QStandardItemModel,
    QStandardItem,
    QBrush,
    QColor,
    QPen,
    QWheelEvent,
)
from PySide2.QtWidgets import (
    QGraphicsScene,
    QGraphicsView,
    QGraphicsLineItem,
    QTreeView,
    QGraphicsPolygonItem,
)
from finesse import BeamParam


class MainWindow:
    def __init__(self, uipath):
        self.uipath = uipath
        self.window = None
        self.scene = None
        self.view = None
        self.component_tree = None
        self.controller = None
        self.beam_path = None
        self._load_ui()
        self._load_controller()

    def _load_ui(self):
        uifile = QFile(str(self.uipath))
        uifile.open(QFile.ReadOnly)
        # Load main window.
        loader = QUiLoader()
        # Register custom UI classes.
        loader.registerCustomWidget(ModeMatchView)
        # Load.
        self.window = loader.load(uifile)
        # Set some useful references.
        self.view = self.window.findChild(QGraphicsView, "graphicsView1")
        self.component_tree = self.window.findChild(QTreeView, "treeView1")
        # Create scene and view.
        self.scene = ModeMatchScene()
        self.view.setScene(self.scene)

    def _load_controller(self):
        self.controller = ModeMatchController(
            self.scene, self.view, self.component_tree
        )

    def show(self):
        self.window.show()


class ModeMatchController:
    """Mode matching controller.

    This handles the mode matching of a model, and displays it on the GUI.
    """

    def __init__(self, scene, view, component_tree, model=None):
        if model is None:
            from finesse.script import parse

            model = parse(
                """
                l L0 P=1
                s s0 L0.p1 M1.p1 L=2.5
                lens M1 f=-2.5
                s s1 M1.p2 M2.p1 L=0.5
                lens M2 f=0.5
                s s2 M2.p2 M3.p1 L=1.0
                lens M3 f=0.5
                s s3 M3.p2 M4.p1 L=1.0
                lens M4 f=0.01
                """
            )
            # model = Model()
            # model.chain(
            #    Laser('lsr'),
            #    0.5,
            #    Lens('l1', f=-2.5),
            #    2.0,
            #    Lens('l2', f=2.5),
            # )

        # GUI stuff.
        self.model = model
        self.scene = scene
        self.view = view
        self.component_tree = component_tree

        self.view.WHEEL_EVENT.connect(self._view_wheel_event)

        # Build component list.
        self.component_tree_model = ComponentTreeModel()
        self.component_tree.setModel(self.component_tree_model)

        self.component_tree.selectionModel().selectionChanged.connect(
            self._tree_items_selected
        )

        # Associate this controller with items.
        self.scene.controller = self

        # Scale factors.
        self.zscale = 100  # pixels per meter
        self.yscale = 20e3  # pixels per meter
        self.grid_step = 0.1 * self.zscale

        # Add beam path to scene.
        self.beam_path = BeamPath(controller=self)
        self.scene.addItem(self.beam_path)

        self._draw_components()
        self.beam_path.recompute()

    @cached_property
    def beam(self):
        start_node = self.model.L0.p1.o
        stop_node = self.model.M4.p1.i
        qstart = BeamParam(w0=1e-3, z=0)
        return self.model.propagate_beam(
            q_in=qstart,
            from_node=start_node,
            to_node=stop_node,
            direction="x",
            symbolic=True,
        )

    @property
    def surfaces(self):
        return self.component_tree_model.ordered_items

    def _draw_components(self):
        for component, position in self.beam.positions.items():
            self._draw_component(component, position)

    def _draw_component(self, component, position):
        zpos = position * self.zscale
        item = Surface(zpos, component=component, controller=self)
        self.component_tree_model.invisibleRootItem().appendRow(item)
        #
        item.SELECT_CHANGE.connect(self._surface_scene_item_select_changed)
        item.POSITION_CHANGE.connect(self._surface_scene_item_position_changed)

        self.scene.addItem(item)

    def _fit_scene(self, xpad=0, ypad=0):
        frame = self.scene.itemsBoundingRect()
        frame += QMarginsF(xpad, ypad, xpad, ypad)
        self.view.setSceneRect(frame)

    def constrain_surface_pos(self, surface, old_pos, new_pos):
        # Don't allow movement in the y-direction.
        new_pos.setY(old_pos.y())
        # Don't allow component to move beyond its neighbours.
        lowerpos, higherpos = self._component_neighbour_xpos(surface)
        if lowerpos is not None and new_pos.x() < lowerpos:
            new_pos.setX(lowerpos)
        elif higherpos is not None and new_pos.x() > higherpos:
            new_pos.setX(higherpos)

        return new_pos

    def _component_neighbour_xpos(self, item):
        items = self.component_tree_model.ordered_items
        nitems = self.component_tree_model.nitems
        previous_pos = None
        next_pos = None
        for i, this_item in enumerate(items):
            if this_item == item:
                if i > 0:
                    previous_pos = items[i - 1].pos().x()
                if i < (nitems - 1):
                    next_pos = items[i + 1].pos().x()
                break
        return previous_pos, next_pos

    def _view_wheel_event(self, item, event):
        """Allow zooming on the view using the cursor position.

        https://stackoverflow.com/questions/19113532/qgraphicsview-zooming-in-and-out-under-mouse-position-using-mouse-wheel
        """
        zoom_in_factor = 1.25
        zoom_out_factor = 1 / zoom_in_factor

        self.view.setTransformationAnchor(QGraphicsView.NoAnchor)
        self.view.setResizeAnchor(QGraphicsView.NoAnchor)

        old_pos = self.view.mapToScene(event.pos())

        if event.delta() > 0:
            zoom_factor = zoom_in_factor
        else:
            zoom_factor = zoom_out_factor

        self.view.scale(zoom_factor, zoom_factor)

        new_pos = self.view.mapToScene(event.pos())

        # Move scene back to old position.
        delta = new_pos - old_pos
        self.view.translate(delta.x(), delta.y())

    def _surface_scene_item_select_changed(self, surface, selected):
        self._set_tree_item_selection(surface, selected)

    def _surface_scene_item_position_changed(self, surface, old_pos, new_pos):
        dz = (old_pos.x() - new_pos.x()) / self.zscale
        lower_space = surface.component.p1.i.space
        upper_space = surface.component.p2.o.space
        if lower_space is not None:
            lower_space.L -= dz
        if upper_space is not None:
            upper_space.L += dz

        self.beam_path.recompute()

    def _tree_items_selected(self, selected, deselected):
        for index in selected.indexes():
            item = self.component_tree_model.itemFromIndex(index)
            item.setSelected(True)
        for index in deselected.indexes():
            item = self.component_tree_model.itemFromIndex(index)
            item.setSelected(False)

    def _set_tree_item_selection(self, item, selected):
        index = self.component_tree_model.indexFromItem(item)
        flag = QItemSelectionModel.Select if selected else QItemSelectionModel.Deselect
        self.component_tree.selectionModel().select(index, flag)

    def select_surface(self, surface, only=True):
        surface.setSelected(True)
        if only:
            for this_surface in self.surfaces:
                if this_surface != surface:
                    this_surface.setSelected(False)


class ModeMatchView(QGraphicsView):
    # Signals
    WHEEL_EVENT = Signal(QGraphicsScene, QWheelEvent)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__mouse_prev_pos = None

    def wheelEvent(self, event):
        self.WHEEL_EVENT.emit(self, event)
        # Don't pass wheel event to parent.
        return

    def mousePressEvent(self, event):
        if event.button() == Qt.MiddleButton:
            # Enter drag mode.
            self.__mouse_prev_pos = event.pos()
            self.setCursor(Qt.ClosedHandCursor)
            return
        super().mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.MiddleButton:
            self.__mouse_prev_pos = None
            self.setCursor(Qt.ArrowCursor)
            return
        super().mouseReleaseEvent(event)

    def mouseMoveEvent(self, event):
        if event.buttons() == Qt.MiddleButton:
            offset = self.__mouse_prev_pos - event.pos()
            # Move view.
            vscroll = self.verticalScrollBar()
            vscroll.setValue(vscroll.value() + offset.y())
            hscroll = self.horizontalScrollBar()
            hscroll.setValue(hscroll.value() + offset.x())
            # Update mouse click position.
            self.__mouse_prev_pos = event.pos()
            return
        super().mouseMoveEvent(event)


class ModeMatchScene(QGraphicsScene):
    def __init__(self, controller=None, **kwargs):
        super().__init__(-1000, -1000, 2000, 2000, **kwargs)
        self.controller = controller

    def drawBackground(self, painter, rect):
        self._draw_gridlines(painter, rect)

    def _draw_gridlines(self, painter, rect):
        """Draw scene gridlines.

        https://www.qtcentre.org/threads/5609-Drawing-grids-efficiently-in-QGraphicsScene
        """
        grid_step = int(self.controller.grid_step)
        painter.setPen(QColor("#eee"))
        left = int(grid_step * round(rect.left() / grid_step))
        top = int(grid_step * round(rect.top() / grid_step))
        lines = []
        for x in range(left, int(rect.right()), grid_step):
            lines.append(QLineF(x, rect.top(), x, rect.bottom()))
        for y in range(top, int(rect.bottom()), grid_step):
            lines.append(QLineF(rect.left(), y, rect.right(), y))
        painter.drawLines(lines)


@total_ordering
class Surface(QGraphicsLineItem, QStandardItem, QObject):
    # Signals
    SELECT_CHANGE = Signal(QGraphicsLineItem, bool)
    POSITION_CHANGE = Signal(QGraphicsLineItem, QPointF, QPointF)

    def __init__(self, zpos, component, controller=None, **kwargs):
        self.component = component
        self.controller = controller
        height = 25.4e-3 * self.controller.yscale

        # WORKAROUND: due to a segmentation fault (https://bugreports.qt.io/browse/PYSIDE-800)
        # we cannot use super() here and must instead explicitly call each parent constructor
        QGraphicsLineItem.__init__(self, **kwargs)
        QStandardItem.__init__(self, self.component.name)
        QObject.__init__(self)

        # Set the line.
        self.setLine(QLineF(0, height / 2, 0, -height / 2))
        pos = self.pos()
        pos.setX(zpos)
        self.setPos(pos)

        pen = QPen()
        pen.setWidth(10)
        pen.setColor(QColor(107, 164, 182, 255 // 2))
        self.setPen(pen)
        self.setFlag(self.ItemIsSelectable)
        self.setFlag(self.ItemIsMovable)
        self.setFlag(self.ItemSendsGeometryChanges)

        # Add branches to the tree item.
        for param in self.component._params:
            row = QStandardItem(f"{param.name} = {param}")
            row.setFlags(~Qt.ItemIsSelectable)
            self.appendRow(row)

    @property
    def zpos(self):
        return self.pos().x()

    def __eq__(self, other):
        return self.component == other.component

    def __ne__(self, other):
        return self.component != other.component

    def __lt__(self, other):
        return self.zpos < other.zpos

    def itemChange(self, change, value):
        if change == self.ItemSelectedChange:
            self.SELECT_CHANGE.emit(self, value)
        elif change == self.ItemPositionChange and self.isSelected():
            value = self.controller.constrain_surface_pos(self, self.pos(), value)
            if self.pos() != value:
                self.POSITION_CHANGE.emit(self, self.pos(), value)

        return super().itemChange(change, value)


class BeamPath(QGraphicsPolygonItem):
    def __init__(self, controller=None, **kwargs):
        super().__init__(**kwargs)
        self.controller = controller

        brush = QBrush(Qt.red)
        brush.setColor(QColor(255, 0, 0, 255 // 5))
        self.setBrush(brush)
        pen = QPen()
        pen.setStyle(Qt.NoPen)
        self.setPen(pen)

    def recompute(self):
        beam = self.controller.beam
        segments = beam.all_segments("beamsize")

        # To create a closed polygon, render top beam edge forwards, then bottom
        # backwards.
        forward = []
        backward = []
        for zs, data in segments.values():
            for z, w in zip(zs, data["beamsize"]):
                z *= self.controller.zscale
                w *= self.controller.yscale
                forward.append(QPointF(z, w))
                backward.append(QPointF(z, -w))

        forward = sorted(forward, key=lambda p: p.x())
        backward = sorted(backward, key=lambda p: p.x())

        self.setPolygon(forward + list(reversed(backward)))


class ComponentTreeModel(QStandardItemModel):
    @property
    def ordered_items(self):
        # ordered by zpos
        items = []
        for i in range(self.nitems):
            items.append(self.item(i))
        # Items implement sort dunder methods.
        return sorted(items)

    @property
    def nitems(self):
        return self.rowCount()
