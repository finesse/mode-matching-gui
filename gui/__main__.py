"""GUI."""

import sys
from pathlib import Path
from PySide2.QtWidgets import QApplication
from .ui import MainWindow

THIS_DIR = Path(__file__).resolve().parent


def load_ui_and_exit():
    UI_PATH = THIS_DIR / "main.ui"
    app = QApplication([])
    main_window = MainWindow(UI_PATH)
    main_window.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    load_ui_and_exit()
